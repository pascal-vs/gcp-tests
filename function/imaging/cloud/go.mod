module vanderswalmen.net/resizer

require (
  github.com/GoogleCloudPlatform/functions-framework-go v1.5.2
  github.com/cloudevents/sdk-go/v2 v2.5.0
  github.com/disintegration/imaging v1.6.2
)
