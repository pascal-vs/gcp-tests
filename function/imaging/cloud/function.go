package resizer

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/disintegration/imaging"
)

const (
	triggerDefault = "full_size"
	replaceDefault = "h200"
	heightDefault  = 200
)

type StorageObjectData struct {
	Bucket         string    `json:"bucket,omitempty"`
	Name           string    `json:"name,omitempty"`
	Metageneration int64     `json:"metageneration,string,omitempty"`
	TimeCreated    time.Time `json:"timeCreated,omitempty"`
	Updated        time.Time `json:"updated,omitempty"`
}

func init() {
	functions.CloudEvent("Resizer", resizer)
}

func resizer(ctx context.Context, e event.Event) error {
	// read config
	trigger, triggerSet := os.LookupEnv("RESIZER_TRIGGER")
	if !triggerSet || trigger == "" {
		trigger = triggerDefault
		log.Printf("'trigger' set to default: '%v'", trigger)
	}
	replace, replaceSet := os.LookupEnv("RESIZER_REPLACE")
	if !replaceSet || replace == "" {
		replace = replaceDefault
		log.Printf("'replace' set to default: '%v'", replace)
	}
	var height int
	heightString, heightSet := os.LookupEnv("RESIZER_HEIGHT")
	if !heightSet || heightString == "" {
		height = heightDefault
		log.Printf("'height' set to default: '%v'", heightDefault)
	} else {
		var errConv error
		height, errConv = strconv.Atoi(heightString)
		if errConv != nil {
			log.Printf("AtoiConv: %v\n", errConv)
			return errConv
		}
	}

	// handle event
	log.Printf("Event ID: %s | Type: %s\n", e.ID(), e.Type())

	var data StorageObjectData
	if errCloudEvent := e.DataAs(&data); errCloudEvent != nil {
		log.Printf("CloudEvent: %v\n", errCloudEvent)
		return errCloudEvent
	}

	// check if triggered for the right path
	var triggerPath strings.Builder
	fmt.Fprintf(&triggerPath, "/%s/", trigger)
	if !strings.Contains(data.Name, triggerPath.String()) {
		errTrigger := fmt.Errorf("path does not contain '/%s/'", trigger)
		log.Printf("Trigger: %v\n", errTrigger)
		return errTrigger
	}

	// start process
	format := filepath.Ext(data.Name)
	log.Printf("Processing: %s/%s (%s)\n", data.Bucket, data.Name, format)

	// prepare source
	client, errClient := storage.NewClient(ctx)
	if errClient != nil {
		log.Printf("NewClient: %v\n", errClient)
		return errClient
	}
	defer client.Close()

	bucketSource := client.Bucket(data.Bucket)
	objectSource := bucketSource.Object(data.Name)

	// prepare destination
	formatSupported, errFormat := imaging.FormatFromExtension(format)
	if errFormat != nil {
		log.Printf("Format: %v\n", errFormat)
		return errFormat
	}
	bucketDestination := client.Bucket(data.Bucket)
	var replacePath strings.Builder
	fmt.Fprintf(&replacePath, "/%s/", replace)
	newPath := strings.Replace(data.Name, triggerPath.String(), replacePath.String(), 1)
	objectDestination := bucketDestination.Object(newPath)
	imageDestination := objectDestination.NewWriter(ctx)

	// read the original image
	imageSource, errRead := objectSource.NewReader(ctx)
	if errRead != nil {
		log.Printf("NewReader: %v\n", errRead)
		return errRead
	}
	image, errDecode := imaging.Decode(imageSource)
	if errDecode != nil {
		log.Printf("Decode: %v\n", errDecode)
		return errDecode
	}

	// process the image
	resized := imaging.Resize(image, 0, height, imaging.Box)

	// write the resized image
	errEncode := imaging.Encode(imageDestination, resized, formatSupported)
	if errEncode != nil {
		log.Printf("Encode: %v\n", errEncode)
		return errEncode
	}

	if errClose := imageDestination.Close(); errClose != nil {
		log.Printf("Close: %v\n", errClose)
		return errClose
	}

	// finished!
	log.Printf("Resized in %s/%s\n", data.Bucket, newPath)

	return nil
}
